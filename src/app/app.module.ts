import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { FormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input'



import { AppComponent } from './app.component';
import { GenComponent } from './components/gen/gen.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CallbackComponent } from './components/callback/callback.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AppRoutingModule } from './app-routing.module';
import { NoPageComponent } from './components//error/no-page/no-page.component';
import { MyEventsComponent } from './components/my-events/my-events.component';
import { MyEventsDetailsComponent } from './components/my-events-details/my-events-details.component'

@NgModule({
  declarations: [
    AppComponent,
    GenComponent,
    CallbackComponent,
    HomeComponent,
    NavbarComponent,
    ProfileComponent,
    NoPageComponent,
    MyEventsComponent,
    MyEventsDetailsComponent,
  ],
  imports: [
    BrowserModule,
    NgxQRCodeModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule
  ],
  providers: [
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
