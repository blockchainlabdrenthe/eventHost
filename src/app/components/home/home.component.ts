import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private auth: AuthService;

  constructor(auth: AuthService) {
    this.setAuth(auth);
  }

  userInfo;
  ngOnInit() {
    this.getAuth().getUser$().subscribe((res: any) => {
      this.userInfo= res;
    });
  }

  public setAuth(auth: AuthService): void {
    this.auth = auth;
  }

  public getAuth(): AuthService {
    return this.auth;
  }

}
