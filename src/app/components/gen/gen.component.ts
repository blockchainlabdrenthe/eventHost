import { Component, OnInit } from '@angular/core';
import { NgxQrcodeElementTypes, NgxQrcodeErrorCorrectionLevels } from '@techiediaries/ngx-qrcode';
import { HttpClient, HttpParams } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from '../../auth/auth.service';
import Auth0Client from '@auth0/auth0-spa-js/dist/typings/Auth0Client';

import { asciiToTrytes, trytesToAscii } from "@iota/converter";
import { composeAPI, generateAddress } from '@iota/core'
//import { create, attach, changeMode, getRoot, init } from '@iota/mam'

import { createChannel, createMessage, parseMessage, mamAttach, mamFetch } from '@iota/mam.js'

import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { stringify } from '@angular/compiler/src/util';
import { formatDate } from '@angular/common';
@Component({
  selector: 'app-gen',
  templateUrl: './gen.component.html',
  styleUrls: ['./gen.component.css'],
  animations: [
    trigger('loginLoader-topbar', [
      state('loaded', style({
        display: "none"
      })),
      state('initial', style({
        display: "flex"
      }))
    ])]

})
export class GenComponent implements OnInit {
  private auth: AuthService;

  elementType = NgxQrcodeElementTypes.URL;
  correctionLevel = NgxQrcodeErrorCorrectionLevels.HIGH;
  myseed: any = '';
  myaddress: any = '';
  eventnumber = 0;
  adresindex_host = 0;
  adresindex_event = 1;
  text_event = "First SSA Event";
  postdata;
  sendresult = "";
  date = new Date().getTime();
  showqr = false;
  loadIcon = false;
  seed: any;
  seedLoader: boolean = false;

  constructor(private httpClient: HttpClient, private spinner: NgxSpinnerService, auth: AuthService) {
    this.setAuth(auth);
  }

  //new event seed
  getNewSeed() {

    const pattern = /([A-Z9])/;

    function getRandomLetter() {
      const byteArray = new Uint8Array(1);
      window.crypto.getRandomValues(byteArray);
      const letter = String.fromCharCode(byteArray[0]);
      return letter.match(pattern) ? letter : getRandomLetter();
    }

    function generateSeed() {
      return new Array(81).fill(0).map(getRandomLetter).join('');
    }
    this.myseed = generateSeed();
    this.seed = true;
  }

  token;
  userInfo;

  ngOnInit() {
    this.getAuth().getUser$().subscribe((res: any) => {
      this.userInfo = res;
    });
    this.auth.auth0Client$.subscribe((client: Auth0Client) => {
      this.token = client.getTokenSilently();
    });
  }

  public setAuth(auth: AuthService): void {
    this.auth = auth;
  }

  public getAuth(): AuthService {
    return this.auth;
  }

  
  publishMAM() {

    this.onGenerateMam().then(result => { });
  }

  onGenerate(event: any) {
    this.loadIcon = true;
    this.showqr = true;
    this.spinner.show();

    const self = this;
    //console.log(options)
    //console.log("http://api.ssa.beeholder.net/Tangle/CreateEvent?eventinfo=" + options);
    const securityLevel = 2;
    const iota = composeAPI({
      provider: 'https://nodes.devnet.iota.org:443'
    });
    const depth = 3;
    const minimumWeightMagnitude = 9;
    this.myaddress = generateAddress(this.myseed, 0, securityLevel);
    console.log(this.myaddress);
    const seed = this.myseed;
    const hostname = this.userInfo.nickname;
    const hostemail = this.userInfo.email;
    const address = this.myaddress;
    const text_event = this.text_event;
    const message = JSON.stringify({ "event": this.text_event, "host": this.userInfo.email});
    const messageInTrytes = asciiToTrytes(message);
    const date = this.date;
    const token = this.token.__zone_symbol__value;
    const httpClient = this.httpClient;
    const transfers = [
      {
        value: 0,
        address: this.myaddress,
        message: messageInTrytes
      }
    ];
    iota.prepareTransfers(this.myseed, transfers)
      .then(trytes => {return iota.sendTrytes(trytes, depth, minimumWeightMagnitude);
      })
      .then(bundle => {
        console.log(bundle[0].hash);
        let options = `{"seed":"${this.myseed}","hostname":"${this.userInfo.nickname}","hostemail":"${this.userInfo.email}","address":"${this.myaddress}","bundlehash":"${bundle[0].hash}","eventname":"${this.text_event}","startdate":"${this.date}","token":"${this.token.__zone_symbol__value}"}`;

        httpClient.post("https://api.ssa.beeholder.net/Tangle/CreateEvent?eventinfo=" + options, '{}').subscribe((data: any) => {
          this.postdata = data;
          this.loadIcon = false;
          this.sendresult = this.myaddress;
          this.spinner.hide();
          console.log(data);
          this.eventnumber = data.result;
        });
      })
      .catch(err => {
        console.error(err)
      });
    /*
    this.httpClient.post("https://api.ssa.beeholder.net/Tangle/CreateEvent?eventinfo=" + options, '{}').subscribe((data: any) => {
      this.postdata = data;
      //console.log("test")
      this.loadIcon = false;
      this.sendresult = data.result;
      this.spinner.hide();
    });
    */
  }

  async publishMessage(mamState, trytesMessage) {

    // //Create MAM Payload
    //const message = create(mamState, trytesMessage);

    //try {
    //  attach(message.payload, message.address, 3, 9);
    //  return message;
    //} catch (err) {

    //}
  }

  async fetchMessages(messageRoot) {

    //try {
    //  let res = fetch(
    //    messageRoot,
    //    "public",
    //    Undefined,
    //  ).then(function (value: any) {
    //    if (value.messages) {
    //      value.messages.forEach((messageTrytes) => {
    //        //logOutput(`Fetched Message: ${trytesToAscii(messageTrytes)}`);
    //      });
    //    }
    //  });
    //  return res;
    //} catch (err) {

    //}
  }

  async onGenerateMam() {
    this.spinner.show();
    let payload = 'Dit is Danny zijn boodschap';
    const mode = 'public';
    const sideKey = 'MYKEY';
    let channelState = createChannel(this.myseed, 2, mode);
    const mamMessage = createMessage(channelState, asciiToTrytes(JSON.stringify(payload)));
    // Display the details for the MAM message.
    console.log('Seed:', channelState.seed);
    console.log('Address:', mamMessage.address);
    console.log('Root:', mamMessage.root);

    console.log('NextRoot:', channelState.nextRoot);
    const api = composeAPI({ provider: "https://nodes.devnet.iota.org:443" });

    // Attach the message.
    console.log('Attaching to tangle, please wait...')
    mamAttach(api, mamMessage, 3, 9, "MY9MAM").then(result => {
      console.log(`You can view the mam channel here https://utils.iota.org/mam/${mamMessage.root}/${mode}/${sideKey}/devnet`);
    });
  }

  async onGenerateMam2() {
    //this.spinner.show();

    //let mamState = init('https://nodes.devnet.iota.org:443', this.myseed);
    //let channelMode = "public"; // "private" "restricted"
    //let retrictedSideKeyTrytes = channelMode === "restricted" ? "THIS9IS9A9RESTRICTED9KEY" : undefined;
    //mamState = changeMode(mamState, "public", retrictedSideKeyTrytes);

    //const message = this.publishMessage(
    //  mamState,
    //  asciiToTrytes(
    //    `Dit is Danny zijn boodschap ${mamState.channel.start + 1}`
    //  )
    //).then(mammes => {
    //  console.log('message published at ' + mammes.address);
    //});
  }

}

