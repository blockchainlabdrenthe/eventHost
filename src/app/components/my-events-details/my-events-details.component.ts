import { Component, OnInit } from '@angular/core';
import { NgxQrcodeElementTypes, NgxQrcodeErrorCorrectionLevels } from '@techiediaries/ngx-qrcode';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-my-events-details',
  templateUrl: './my-events-details.component.html',
  styleUrls: ['./my-events-details.component.css']
})
export class MyEventsDetailsComponent implements OnInit {

  public eventsId:string;
  public eventname: string;
  elementType = NgxQrcodeElementTypes.URL;
  correctionLevel = NgxQrcodeErrorCorrectionLevels.HIGH;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.eventsId = this.route.snapshot.params['id'];
    this.eventname = this.route.snapshot.params['name'];
    this.route.params.subscribe((result)=>{
      this.eventsId = result['id'];
    })
  }

}
