import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-my-events',
  templateUrl: './my-events.component.html',
  styleUrls: ['./my-events.component.css']
})
export class MyEventsComponent implements OnInit {
  private auth: AuthService;

  public userInfo;
  public events;

  constructor(auth: AuthService, public http:HttpClient) {
    this.setAuth(auth);
    this.userInfo = null;
    this.events = null;
  }

  ngOnInit() {

    this.getAuth().getUser$().subscribe((res: any) => {
      this.userInfo = res.email;
      //console.log(this.userInfo);

      this.http.get('https://api.ssa.beeholder.net/Tangle/GetEvents?hostemail='+this.userInfo).subscribe((res: any) => {
        this.events = res;
        //console.log(this.events);
        if(this.events === "GetEvents Exception Object reference not set to an instance of an object., awd@awd.nl"){
          this.events = undefined;
        }
     });
    });
  }

  public setAuth(auth: AuthService): void {
    this.auth = auth;
  }

  public getAuth(): AuthService {
    return this.auth;
  }
}
