import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  private auth: AuthService;
  constructor(
    auth: AuthService,)
    {
    this.setAuth(auth);
    }

  userInfo;
  ngOnInit() {
    this.getAuth().getUser$().subscribe((res: any) => {
      this.userInfo= res;
    });
  }

  public setAuth(auth: AuthService): void {
    this.auth = auth;
  }

  public getAuth(): AuthService {
    return this.auth;
  }
}
