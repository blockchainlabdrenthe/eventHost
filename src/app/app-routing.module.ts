import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CallbackComponent } from './components/callback/callback.component';
import { HomeComponent } from './components/home/home.component';
import { GenComponent } from './components/gen/gen.component';
import { AuthGuard } from './auth/auth.guard';
import { ProfileComponent } from './components/profile/profile.component';
import { NoPageComponent } from './components/error/no-page/no-page.component';
import { MyEventsComponent } from './components/my-events/my-events.component';
import { MyEventsDetailsComponent } from './components/my-events-details/my-events-details.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'generate',
    component: GenComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'myevents',
    component: MyEventsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'myevents/:id/:name',
    component: MyEventsDetailsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'callback',
    component: CallbackComponent
  },
  {
    path: '**',
    component: NoPageComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
